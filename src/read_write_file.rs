use std::fs::File;
use std::io::{BufRead, BufReader, Write};

pub struct Participants {
    rut: String, 
    name: String,
    ticket: u8,
} 


fn add_path_data(name_file: String) -> String {
    let directory_path: String = format!("data/{}", name_file);
    directory_path
}


fn save_data_in_vec(table_to_save: &mut Vec<Participants>, reader: &mut BufReader<File>) {
    for line in reader.lines() {
        if let Ok(line) = line {
            let parts: Vec<&str> = line.split(',').collect();
            if parts.len() == 3 {
                let rut: String = parts[0].to_string();
                let name: String = parts[1].to_string();
                if let Ok(ticket) = parts[2].trim().parse::<u8>() {
                    let participant: Participants = Participants { rut: rut, name: name, ticket: ticket };
                    table_to_save.push(participant);
                } else {
                    println!("Error: Could not parse ticket number");
                }
            } else {
                println!("Error: Invalid line format");
            }
        } else {
            println!("Error: Failed to read line");
        }
    }
}


pub fn read_file(name_file: String) -> Vec<Participants> {
    let file: File = File::open(add_path_data(name_file)).expect("Error to open the file");
    let mut reader: BufReader<File> = BufReader::new(file);
    let mut people: Vec<Participants> = Vec::new();
    save_data_in_vec(&mut people, &mut reader);
    people
}

pub fn change_extension_to_sal(name_file: String) -> String {
    let parts: Vec<&str> = name_file.split('.').collect();
    let new_name_sal: String = format!("{}.sal", parts[0]);
    new_name_sal
}


fn ticket_penalty(ticket: u8) -> u8 {
    if ticket >= 1 && ticket <= 2 {
        return ticket;
    } else if ticket >= 3 && ticket <= 7 {
        return 2;
    } else if ticket >= 8 && ticket <= 15 {
        return  1;
    } else {
        return 0;
    }
}


fn search_the_same_rut_for_add_tickets(mut participant: Vec<Participants>) -> Vec<Participants> {
    let mut index: usize = 0;
    while index < participant.len() {
        let mut second_index: usize = index + 1;
        while second_index < participant.len() {
            if participant[index].rut == participant[second_index].rut {
                participant[index].ticket += participant[second_index].ticket;
                participant.remove(second_index);
                second_index -= 1;
            }
            second_index += 1;
        }
        index += 1;
    }
    participant
}


fn apply_penalty_to_tickets(list_of_participants:&mut Vec<Participants>) {
    let mut index: usize = 0;
    while index < list_of_participants.len() {
        list_of_participants[index].ticket = ticket_penalty(list_of_participants[index].ticket);
        if list_of_participants[index].ticket == 0 {
            list_of_participants.remove(index);
            index -= 1;
        }
        index += 1;
    }
}


fn get_assigned_and_pending(participants: &Vec<Participants>) -> String {
    let mut sum_of_tickets: u8 = 0;
    for user in participants {
        sum_of_tickets += user.ticket;
    }
    let tickets_pending: u8 = 200 - sum_of_tickets;
    let stastic: String = format!("Assigned: {} \nPending: {} \n", sum_of_tickets, tickets_pending);
    stastic
}


pub fn create_and_write_file(list_of_data: Vec<Participants>, name_file: String) {
    let path_file: String = add_path_data(change_extension_to_sal(name_file));
    let mut sal_file: File = File::create(path_file).expect("Fail in creation");
    let mut new_list_of_participant: Vec<Participants> = search_the_same_rut_for_add_tickets(list_of_data);
    apply_penalty_to_tickets(&mut new_list_of_participant);
    for participant in &new_list_of_participant {
        let line: String = format!("{},{},{}\n", participant.rut, participant.name, participant.ticket);
        sal_file.write_all(line.as_bytes()).unwrap(); 
    }
    let assigned_and_pending: String = get_assigned_and_pending(&new_list_of_participant);
    sal_file.write_all(assigned_and_pending.as_bytes()).unwrap();
}
