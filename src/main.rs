mod input_terminal;
mod read_write_file;
use input_terminal::input_string;
use read_write_file::{change_extension_to_sal, create_and_write_file, read_file, Participants};
use std::time::{Duration, Instant};

fn interface() -> () {
    println!("Welcome to Lollapaloza");
    let name_file: String = input_string("Enter the name of the file: ");
    let start_time_reading: Instant = Instant::now();
    let data_of_file: Vec<Participants> = read_file(name_file.clone());
    let end_time_reading: Duration = start_time_reading.elapsed();
    let start_writing: Instant = Instant::now();
    create_and_write_file(data_of_file, name_file.clone());
    let end_writing: Duration = start_writing.elapsed();
    println!(
        "The file {} was created",
        change_extension_to_sal(name_file.clone())
    );
    println!(
        "\nTime to read a file: {} \nTime to write the file: {}",
        end_time_reading.as_micros(),
        end_writing.as_micros()
    );
}

fn main() {
    interface();
}
